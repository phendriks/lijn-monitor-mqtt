﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.Windows.Forms;

namespace LijnMonitorMqtt
{
    public partial class MainForm : Form
    {
        MqttConnection mqttc;
        List<ConfigLijn> cl = new List<ConfigLijn>();
        private static bool _isAdminOrSupervisor = false;
        public static bool isAdminOrSupervisor { get { return _isAdminOrSupervisor; } set { return; } }

        public MainForm()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            FormClosing += Form1_FormClosing;
            Move += ActiveForm_Move;


            //LijnenView.Columns["ready"].Visible = false;
            //LijnenView.Columns["ingesprek"].Visible = false;
            //LijnenView.Columns["ingelogd"].Visible = false;
            LijnenView.Columns[5].Visible = false;
            LijnenView.Columns[6].Visible = false;
            LijnenView.Columns[7].Visible = false;

            for(int i = 1; i<=7; i++)
            {
                LijnenView.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
            }


            if (IsUserInGroup(System.Security.Principal.WindowsIdentity.GetCurrent().Name, "G_APP_Lijnmonitor_Supervisor"))
            {
                _isAdminOrSupervisor = true;
            }
            updateDataCallback udcb = new updateDataCallback(updateData);
            this.mqttc = new MqttConnection("la-dc05", "", udcb);

        }
        public delegate void updateDataCallback(string topic, string e);
        void updateData(string topic, string e)
        {
            if (topic.Contains("lijnen"))
            {
                Lijn l = JsonConvert.DeserializeObject<Lijn>(e);
                Debug.WriteLine(l);
                

                this.updateBindingSource(l);
            }
            else if (topic.Contains("last_update"))
            {
                DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                dateTime = dateTime.AddSeconds(double.Parse(e));
                Debug.WriteLine("Last update: " + dateTime.ToLocalTime());
                updateLabel(dateTime.ToLocalTime().ToString());
            }
            else if (topic.Contains("config"))
            {
                var cll = JsonConvert.DeserializeObject<ConfigLijnList>(e);
                Debug.WriteLine(cll);

                ClearAndUpdateBindingSource(cll);

                mqttc.CleanAndResubscribe(cll);
            }
            else
            {
                Debug.WriteLine(e);
            }
        }
        public delegate void ClearAndUpdateBindingSourceCallback(ConfigLijnList cll);
        public void ClearAndUpdateBindingSource(ConfigLijnList cll)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new ClearAndUpdateBindingSourceCallback(ClearAndUpdateBindingSource), cll);
            }
            else
            {
                lijnenBindingSource.Clear();
                foreach (ConfigLijn cl in cll.lijnen)
                {
                    Lijn tmpLijn = new Lijn() { displayname = cl.shortname, naam = cl.id };
                    lijnenBindingSource.Add(tmpLijn);
                }
            }
        }

        public delegate void UpdateBindingSourceCallback(Lijn l);
        public void updateBindingSource(Lijn l)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new UpdateBindingSourceCallback(updateBindingSource), l);
            }
            else
            {
                int index = lijnenBindingSource.List.IndexOf(l);
                if (index == -1)
                {
                    lijnenBindingSource.Add(l);
                }
                else
                {
                    Lijn changes = (Lijn)lijnenBindingSource.List[index];
                    changes.avgwachttijd = l.avgwachttijd;
                    changes.callshandled = l.callshandled;
                    changes.ingelogd = l.ingelogd;
                    changes.ingesprek = l.ingesprek;
                    changes.ready = l.ready;
                    changes.wachtrij = l.wachtrij;
                    changes.wachttijd = l.wachttijd;

                    lijnenBindingSource.List[index] = changes;
                }

            }
        }


        private delegate void UpdateLabelCallback(string t);
        private void updateLabel(string t)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateLabelCallback(updateLabel), new string[] { t });
            }
            else
            {
                lblUpdateTime.Text = t;
            }
        }

        private void ActiveForm_Move(object sender, EventArgs e)
        {
            KeepBounds();
        }
        private void KeepBounds()
        {
            if (this.Left < SystemInformation.VirtualScreen.Left)
                this.Left = SystemInformation.VirtualScreen.Left;

            if (this.Right > SystemInformation.VirtualScreen.Right)
                this.Left = SystemInformation.VirtualScreen.Right - this.Width;

            if (this.Top < SystemInformation.VirtualScreen.Top)
                this.Top = SystemInformation.VirtualScreen.Top;

            if (this.Bottom > SystemInformation.VirtualScreen.Bottom)
                this.Top = SystemInformation.VirtualScreen.Bottom - this.Height;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            closeProgram(e);
        }



        public static void closeProgram(FormClosingEventArgs e)
        {
            if (isAdminOrSupervisor)
            {
                Environment.Exit(0);
            }
            else
            {
                e.Cancel = true;
            }

            if (e.CloseReason == CloseReason.WindowsShutDown)
            {
                Application.Exit();
            }
            else if (e.CloseReason == CloseReason.ApplicationExitCall)
            {
                Application.Exit();
            }
            else
            {
                
            }
        }

        public bool IsUserInGroup(string userName, string groupName)
        {
            using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain))
            {
                UserPrincipal user = UserPrincipal.FindByIdentity(ctx, userName);

                if (user != null)
                {
                    var groups = user.GetAuthorizationGroups();

                    foreach (GroupPrincipal group in groups)
                    {
                        if (group.Name == groupName)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        
        public void ResizeWindow()
        {
            var w = LijnenView.Columns.GetColumnsWidth(DataGridViewElementStates.Displayed);
            splitView.Width = w;
            this.Width = w + 23;
            var h = LijnenView.Rows.GetRowsHeight(DataGridViewElementStates.Visible);
            this.Height = h + 84;
        }

        private void LijnenView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            LijnenView.ClearSelection();
            ResizeWindow();
        }

        private void MainForm_DoubleClick(object sender, EventArgs e)
        {
            elevatePriv();
        }

        private void LijnenView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            elevatePriv();
        }

        private void elevatePriv()
        {
            if (!isAdminOrSupervisor)
            {
                ProcessStartInfo proc = new ProcessStartInfo();
                proc.UseShellExecute = true;
                proc.WorkingDirectory = Environment.CurrentDirectory;
                proc.FileName = Application.ExecutablePath;
                proc.Verb = "runas";


                try
                {
                    Process.Start(proc);
                }
                catch
                {
                    return;
                }
            }
        }

        private void LijnenView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            elevatePriv();
        }

        private void LijnenView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (dgv.Columns[e.ColumnIndex].Name == "avgwachttijd" || dgv.Columns[e.ColumnIndex].Name == "wachttijd")
            {

                TimeSpan ts = new TimeSpan();
                ts=TimeSpan.FromMilliseconds((int) e.Value);
                e.Value = ts.ToString(@"mm\:ss");
            }
            if(dgv.Columns[e.ColumnIndex].Name == "wachtrij")
            {
                if((int)e.Value > 0)
                {
                    dgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = System.Drawing.Color.Red;

                }
                else
                {
                    dgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                }
            }
        }
    }
}
