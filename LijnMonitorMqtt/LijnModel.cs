﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;

namespace LijnMonitorMqtt
{
    public class Lijnen : ObservableCollection<Lijn>
    {

    }

    public class Lijn : IEquatable<Lijn>
    {
        private int _wachttijd;
        private string _naam;

        [Browsable(false)]
        public string naam { get { return this._naam; } set { if (displayname == null) this.displayname = value; this._naam = value; } }

        [DisplayName("Naam")]
        public string displayname { get; set; }

        [DisplayName("Wachtrij")]
        public int wachtrij { get; set; }

        [DisplayName("Wachttijd")]
        public int wachttijd
        {
            get { return this._wachttijd; }
            set { this._wachttijd = value; }
        }

        [DisplayName("Gem. wachttijd"), JsonProperty("avgwachttijd")]
        public int avgwachttijd
        {
            get;set;
        }

        


        [DisplayName("Totaal"), ]
        public int callshandled { get; set; }

        [DisplayName("Ingelogd")]
        public int ingelogd { get; set; }

        [DisplayName("Beschikbaar")]
        public int ready { get; set; }

        [DisplayName("Ingesprek")]
        public int ingesprek { get; set; }

        public override string ToString()
        {
            return String.Format("{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}", naam, wachtrij, wachttijd, avgwachttijd, callshandled, ingelogd, ready, ingesprek);
        }

        public override int GetHashCode()
        {
            return naam.GetHashCode();
        }


        public bool Equals(Lijn other)
        {
            if (Object.ReferenceEquals(other, null)) return false;
            if (Object.ReferenceEquals(this, other)) return true;
            return (naam.Equals(other.naam));
        }

    }

    public class ConfigLijnList
    {
        [JsonProperty("lijnen")]
        public List<ConfigLijn> lijnen { get; set; }

    }

    public class ConfigLijn
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("shortname")]
        public string shortname { get; set; }
    }
}
