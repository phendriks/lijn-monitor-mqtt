﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LijnMonitorMqtt
{
    partial class MainForm
    {
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1); 
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                //cp.Style |= WS_SYSMENU;
                cp.Style &= ~WS_SYSMENU; // Deze verbergt alle knoppen bovenin inclusief de close button.
                return cp;
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitView = new System.Windows.Forms.TableLayoutPanel();
            this.LijnenView = new System.Windows.Forms.DataGridView();
            this.lijnenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblUpdateTime = new System.Windows.Forms.Label();
            this.displayname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wachtrij = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wachttijd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.displaynameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wachtrijDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wachttijdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.avgwachttijdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.callshandledDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingelogdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.readyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingesprekDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LijnenView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lijnenBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // splitView
            // 
            this.splitView.ColumnCount = 1;
            this.splitView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.splitView.Controls.Add(this.LijnenView, 0, 1);
            this.splitView.Controls.Add(this.lblUpdateTime, 0, 0);
            this.splitView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitView.Location = new System.Drawing.Point(0, 0);
            this.splitView.Name = "splitView";
            this.splitView.RowCount = 2;
            this.splitView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.splitView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.splitView.Size = new System.Drawing.Size(622, 153);
            this.splitView.TabIndex = 0;
            // 
            // LijnenView
            // 
            this.LijnenView.AllowUserToAddRows = false;
            this.LijnenView.AllowUserToDeleteRows = false;
            this.LijnenView.AllowUserToResizeColumns = false;
            this.LijnenView.AllowUserToResizeRows = false;
            this.LijnenView.AutoGenerateColumns = false;
            this.LijnenView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.LijnenView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.LijnenView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LijnenView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LijnenView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.displaynameDataGridViewTextBoxColumn,
            this.wachtrijDataGridViewTextBoxColumn,
            this.wachttijdDataGridViewTextBoxColumn,
            this.avgwachttijdDataGridViewTextBoxColumn,
            this.callshandledDataGridViewTextBoxColumn,
            this.ingelogdDataGridViewTextBoxColumn,
            this.readyDataGridViewTextBoxColumn,
            this.ingesprekDataGridViewTextBoxColumn});
            this.LijnenView.DataSource = this.lijnenBindingSource;
            this.LijnenView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LijnenView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.LijnenView.Enabled = false;
            this.LijnenView.EnableHeadersVisualStyles = false;
            this.LijnenView.Location = new System.Drawing.Point(3, 23);
            this.LijnenView.MultiSelect = false;
            this.LijnenView.Name = "LijnenView";
            this.LijnenView.ReadOnly = true;
            this.LijnenView.RowHeadersVisible = false;
            this.LijnenView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.LijnenView.ShowCellErrors = false;
            this.LijnenView.ShowCellToolTips = false;
            this.LijnenView.ShowEditingIcon = false;
            this.LijnenView.ShowRowErrors = false;
            this.LijnenView.Size = new System.Drawing.Size(616, 127);
            this.LijnenView.TabIndex = 6;
            this.LijnenView.TabStop = false;
            this.LijnenView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.LijnenView_CellContentClick);
            this.LijnenView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.LijnenView_CellFormatting);
            this.LijnenView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.LijnenView_DataBindingComplete);
            this.LijnenView.DoubleClick += new System.EventHandler(this.MainForm_DoubleClick);
            this.LijnenView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LijnenView_MouseDoubleClick);
            // 
            // lijnenBindingSource
            // 
            this.lijnenBindingSource.DataSource = typeof(LijnMonitorMqtt.Lijnen);
            // 
            // lblUpdateTime
            // 
            this.lblUpdateTime.AutoSize = true;
            this.lblUpdateTime.Location = new System.Drawing.Point(3, 5);
            this.lblUpdateTime.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.lblUpdateTime.Name = "lblUpdateTime";
            this.lblUpdateTime.Size = new System.Drawing.Size(61, 13);
            this.lblUpdateTime.TabIndex = 2;
            this.lblUpdateTime.Text = "Connecting";
            //// 
            //// displayname
            //// 
            //this.displayname.DataPropertyName = "displayname";
            //this.displayname.HeaderText = "Naam";
            //this.displayname.Name = "displayname";
            //this.displayname.Width = 60;
            //// 
            //// wachtrij
            //// 
            //this.wachtrij.DataPropertyName = "wachtrij";
            //dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            //this.wachtrij.DefaultCellStyle = dataGridViewCellStyle1;
            //this.wachtrij.HeaderText = "Wachtrij";
            //this.wachtrij.Name = "wachtrij";
            //this.wachtrij.Width = 71;
            //// 
            //// wachttijd
            //// 
            //this.wachttijd.DataPropertyName = "wachttijd";
            //dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            //this.wachttijd.DefaultCellStyle = dataGridViewCellStyle2;
            //this.wachttijd.HeaderText = "Wachttijd";
            //this.wachttijd.Name = "wachttijd";
            //this.wachttijd.Width = 77;
            // 
            // displaynameDataGridViewTextBoxColumn
            // 
            this.displaynameDataGridViewTextBoxColumn.DataPropertyName = "displayname";
            this.displaynameDataGridViewTextBoxColumn.HeaderText = "Naam";
            this.displaynameDataGridViewTextBoxColumn.Name = "displaynameDataGridViewTextBoxColumn";
            this.displaynameDataGridViewTextBoxColumn.ReadOnly = true;
            this.displaynameDataGridViewTextBoxColumn.Width = 60;
            // 
            // wachtrijDataGridViewTextBoxColumn
            // 
            this.wachtrijDataGridViewTextBoxColumn.DataPropertyName = "wachtrij";
            this.wachtrijDataGridViewTextBoxColumn.HeaderText = "Wachtrij";
            this.wachtrijDataGridViewTextBoxColumn.Name = "wachtrij";
            this.wachtrijDataGridViewTextBoxColumn.ReadOnly = true;
            this.wachtrijDataGridViewTextBoxColumn.Width = 71;
            // 
            // wachttijdDataGridViewTextBoxColumn
            // 
            this.wachttijdDataGridViewTextBoxColumn.DataPropertyName = "wachttijd";
            this.wachttijdDataGridViewTextBoxColumn.HeaderText = "Wachttijd";
            this.wachttijdDataGridViewTextBoxColumn.Name = "wachttijd";
            this.wachttijdDataGridViewTextBoxColumn.ReadOnly = true;
            this.wachttijdDataGridViewTextBoxColumn.Width = 77;
            // 
            // avgwachttijdDataGridViewTextBoxColumn
            // 
            this.avgwachttijdDataGridViewTextBoxColumn.DataPropertyName = "avgwachttijd";
            this.avgwachttijdDataGridViewTextBoxColumn.HeaderText = "Gem. wachttijd";
            this.avgwachttijdDataGridViewTextBoxColumn.Name = "avgwachttijd";
            this.avgwachttijdDataGridViewTextBoxColumn.ReadOnly = true;
            this.avgwachttijdDataGridViewTextBoxColumn.Width = 102;
            // 
            // callshandledDataGridViewTextBoxColumn
            // 
            this.callshandledDataGridViewTextBoxColumn.DataPropertyName = "callshandled";
            this.callshandledDataGridViewTextBoxColumn.HeaderText = "Totaal";
            this.callshandledDataGridViewTextBoxColumn.Name = "callshandled";
            this.callshandledDataGridViewTextBoxColumn.ReadOnly = true;
            this.callshandledDataGridViewTextBoxColumn.Width = 62;
            // 
            // ingelogdDataGridViewTextBoxColumn
            // 
            this.ingelogdDataGridViewTextBoxColumn.DataPropertyName = "ingelogd";
            this.ingelogdDataGridViewTextBoxColumn.HeaderText = "Ingelogd";
            this.ingelogdDataGridViewTextBoxColumn.Name = "ingelogd";
            this.ingelogdDataGridViewTextBoxColumn.ReadOnly = true;
            this.ingelogdDataGridViewTextBoxColumn.Width = 73;
            // 
            // readyDataGridViewTextBoxColumn
            // 
            this.readyDataGridViewTextBoxColumn.DataPropertyName = "ready";
            this.readyDataGridViewTextBoxColumn.HeaderText = "Beschikbaar";
            this.readyDataGridViewTextBoxColumn.Name = "ready";
            this.readyDataGridViewTextBoxColumn.ReadOnly = true;
            this.readyDataGridViewTextBoxColumn.Width = 91;
            // 
            // ingesprekDataGridViewTextBoxColumn
            // 
            this.ingesprekDataGridViewTextBoxColumn.DataPropertyName = "ingesprek";
            this.ingesprekDataGridViewTextBoxColumn.HeaderText = "Ingesprek";
            this.ingesprekDataGridViewTextBoxColumn.Name = "ingesprek";
            this.ingesprekDataGridViewTextBoxColumn.ReadOnly = true;
            this.ingesprekDataGridViewTextBoxColumn.Width = 79;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 153);
            this.Controls.Add(this.splitView);
            this.Name = "MainForm";
            this.Text = "Lijn Monitor";
            this.DoubleClick += new System.EventHandler(this.MainForm_DoubleClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LijnenView_MouseDoubleClick);
            this.splitView.ResumeLayout(false);
            this.splitView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LijnenView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lijnenBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel splitView;
        public Label lblUpdateTime;
        private BindingSource lijnenBindingSource;
        public DataGridView LijnenView;
        private DataGridViewTextBoxColumn avgwachttijd;
        private DataGridViewTextBoxColumn displayname;
        private DataGridViewTextBoxColumn wachtrij;
        private DataGridViewTextBoxColumn wachttijd;
        private DataGridViewTextBoxColumn displaynameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn wachtrijDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn wachttijdDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn avgwachttijdDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn callshandledDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn ingelogdDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn readyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn ingesprekDataGridViewTextBoxColumn;
    }
}

