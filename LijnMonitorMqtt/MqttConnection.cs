﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Exceptions;

namespace LijnMonitorMqtt
{
    class MqttConnection
    {
        MqttClient mc;
        MainForm.updateDataCallback ucb;

        public MqttConnection(string host,string configname, MainForm.updateDataCallback udcb)
        {
            try
            {
                mc = new MqttClient(host);
                mc.MqttMsgPublishReceived += Mc_MqttMsgPublishReceived;
                mc.MqttMsgSubscribed += Mc_MqttMsgSubscribed;
                mc.MqttMsgPublished += Mc_MqttMsgPublished;
                configname = (configname == "") ? "+" : configname;
                this.ucb = udcb;

                mc.Connect(Guid.NewGuid().ToString());
                mc.Subscribe(new String[] { "config/+", "last_update", }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                
            }
            catch(Exception mce)
            {
                Debug.WriteLine(mce.Message);
            }
        }

        private void Mc_MqttMsgPublished(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void Mc_MqttMsgSubscribed(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgSubscribedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void Mc_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            string msg = Encoding.UTF8.GetString(e.Message);
            ucb.Invoke(e.Topic, msg);
        }

        public void CleanAndResubscribe(ConfigLijnList cll)
        {
            mc.Unsubscribe(new string[]{"lijnen/#"});

            

            List<byte> qos = new List<byte>();
            List<String> topics = new List<string>();
            foreach (ConfigLijn cl in cll.lijnen)
            {
                
                topics.Add("lijnen/"+cl.id);
                qos.Add(MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE);
            }

            mc.Subscribe(topics.ToArray(), qos.ToArray());
        }
    }
}
